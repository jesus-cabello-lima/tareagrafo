
// create a graph class 
class Graph { 
    // defining vertex array and 
    // adjacent list 
    constructor(noOfVertices) 
    { 
        this.noOfVertices = noOfVertices; 
        this.AdjList = new Map(); 
    } 
    
    // add vertex to the graph 
    addVertex(v) { 
        // initialize the adjacent list with a 
        // null array 
        this.AdjList.set(v, []); 
    }  

    // add edge to the graph 
    addEdge(v, w) { 
        // get the list for vertex v and put the 
        // vertex w denoting edge between v and w 
        this.AdjList.get(v).push(w); 

        // Since graph is undirected, 
        // add an edge from w to v also 
        this.AdjList.get(w).push(v); 
    }

    // Prints the vertex and adjacency list 
    printGraph() 
    { 
        // get all the vertices 
        var get_keys = this.AdjList.keys(); 
    
        // iterate over the vertices 
        for (var i of get_keys)  
    { 
            // great the corresponding adjacency list 
            // for the vertex 
            var get_values = this.AdjList.get(i); 
            var conc = ""; 
    
            // iterate over the adjacency list 
            // concatenate the values into a string 
            for (var j of get_values) 
                conc += j + " "; 
    
            // print the vertex and its adjacency list 
            console.log(i + " -> " + conc); 
        } 
    } 
  
    // function to performs BFS 
    bfs(startingNode) { 
    
        // create a visited array 
        var visited = []; 
        for (var i = 0; i < this.noOfVertices; i++) 
            visited[i] = false; 
    
        // Create an object for queue 
        var q = new Queue(); 
    
        // add the starting node to the queue 
        visited[startingNode] = true; 
        q.enqueue(startingNode); 
    
        // loop until queue is element 
        while (!q.isEmpty()) { 
            // get the element from the queue 
            var getQueueElement = q.dequeue(); 
            
            // passing the current vertex to callback funtion 
            console.log(getQueueElement);

            // get the adjacent list for current vertex 
            var get_List = this.AdjList.get(getQueueElement); 
    
            // loop through the list and add the element to the 
            // queue if it is not processed yet 
            for (var i in get_List) { 
                var neigh = get_List[i]; 
    
                if (!visited[neigh]) { 
                    visited[neigh] = true; 
                    q.enqueue(neigh); 
                } 
            } 
        } 
    } 

    // function to performs BFS 
    bfsDefined(startingNode, lastNode) { 
    
        // create a visited array 
        var visited = []; 
        for (var i = 0; i < this.noOfVertices; i++) 
            visited[i] = false; 
    
        // Create an object for queue 
        var q = new Queue(); 
    
        // add the starting node to the queue 
        visited[startingNode] = true; 
        q.enqueue(startingNode); 
    
        // loop until queue is element 
        while (!q.isEmpty()) { 
            // get the element from the queue 
            var getQueueElement = q.dequeue(); 
            
            // passing the current vertex to callback funtion 
            console.log(getQueueElement);

            if(getQueueElement == lastNode)
                break;
    
            // get the adjacent list for current vertex 
            var get_List = this.AdjList.get(getQueueElement); 
    
            // loop through the list and add the element to the 
            // queue if it is not processed yet 
            for (var i in get_List) { 
                var neigh = get_List[i]; 
    
                if (!visited[neigh]) { 
                    visited[neigh] = true; 
                    q.enqueue(neigh); 
                } 
            } 
        } 
    } 
    
    // Main DFS method 
    dfs(startingNode) 
    { 
    
        var visited = []; 
        for (var i = 0; i < this.noOfVertices; i++) 
            visited[i] = false; 
    
        this.DFSUtil(startingNode, visited); 
    } 
    
    // Recursive function which process and explore 
    // all the adjacent vertex of the vertex with which it is called 
    DFSUtil(vert, visited) 
    { 
        visited[vert] = true; 
        console.log(vert); 
    
        var get_neighbours = this.AdjList.get(vert); 
    
        for (var i in get_neighbours) { 
            var get_elem = get_neighbours[i]; 
            if (!visited[get_elem]) 
                this.DFSUtil(get_elem, visited); 
        } 
    }
    
    // Main DFS method 
    dfsDefined(startingNode, lastNode) { 
        var visited = []; 
        for (var i = 0; i < this.noOfVertices; i++) 
            visited[i] = false; 
    
        this.DFSUtilDefined(startingNode, visited, lastNode); 
    } 
    
    // Recursive function which process and explore 
    // all the adjacent vertex of the vertex with which it is called 
    DFSUtilDefined(vert, visited, lastNode) { 
        visited[vert] = true; 
        
        if(vert == lastNode){
            console.log(vert); 
            return;
        }else
            console.log(vert);
        
        
        var get_neighbours = this.AdjList.get(vert); 
    
        for (var i in get_neighbours) { 
            var get_elem = get_neighbours[i]; 
            if (!visited[get_elem]){
                this.DFSUtilDefined(get_elem, visited, lastNode);
            } 
        } 
    }  
} 

// Using the above implemented graph class 
var g = new Graph(20); 
var vertices = [ 'Oradea',
                 'Zerind',
                 'Arad',
                 'Timisoara',
                 'Lugoj',
                 'Mehadia',
                 'Drobeta',
                 'Sibiu',
                 'Rimnicu_Vilcea',
                 'Craiova',
                 'Fagaras',
                 'Pitesti',
                 'Bucharest',
                 'Giurgiu',
                 'Urziceni',
                 'Vaslui',
                 'Iasi',
                 'Neamt',
                 'Hirsova',
                 'Eforie' ]; 
  
// adding vertices 
for (var i = 0; i < vertices.length; i++) { 
    g.addVertex(vertices[i]); 
} 
  
// adding edges 
g.addEdge('Oradea', 'Zerind'); 
g.addEdge('Oradea', 'Sibiu');
g.addEdge('Zerind', 'Arad'); 
g.addEdge('Arad', 'Sibiu');
g.addEdge('Arad', 'Timisoara');
g.addEdge('Timisoara', 'Lugoj');
g.addEdge('Lugoj', 'Mehadia');
g.addEdge('Mehadia', 'Drobeta');
g.addEdge('Drobeta', 'Craiova');
g.addEdge('Craiova', 'Rimnicu_Vilcea');
g.addEdge('Rimnicu_Vilcea', 'Sibiu');
g.addEdge('Sibiu', 'Fagaras');
g.addEdge('Rimnicu_Vilcea', 'Pitesti');
g.addEdge('Pitesti', 'Bucharest');
g.addEdge('Fagaras', 'Bucharest');
g.addEdge('Bucharest', 'Giurgiu');
g.addEdge('Bucharest', 'Urziceni');
g.addEdge('Urziceni', 'Hirsova');
g.addEdge('Hirsova', 'Eforie');
g.addEdge('Urziceni', 'Vaslui');
g.addEdge('Vaslui', 'Iasi');
g.addEdge('Iasi', 'Neamt');

  
// prints all vertex and 
// its adjacency list 
console.log('--------------------');
console.log('Impresion del grafo');
g.printGraph();

console.log('--------------------');
console.log('Recorrido en anchura');
g.bfsDefined('Oradea', 'Bucharest');
console.log('--------------------');
console.log('Recorrido en profundidad');
g.dfsDefined('Oradea', 'Bucharest');
